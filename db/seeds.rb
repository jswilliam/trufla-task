# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
5.times do
  Department.create(name: Faker::Commerce.department)
end

10.times do
  Promotion.create(
    code: Faker::Commerce.promotion_code,
    discount: Faker::Commerce.price(range = 0..70.0),
    active: [true, false].sample
  )
end

100.times do
  Product.create(
    name: Faker::Commerce.product_name,
    price: Faker::Commerce.price,
    department: Department.find(rand(1..5)),
    promotions: [Promotion.find(rand(1..10))]
  )
end
