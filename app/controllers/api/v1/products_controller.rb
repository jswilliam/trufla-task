class Api::V1::ProductsController < ApplicationController
  # GET /products
  def index
    if params[:page] && params[:page][:number] && params[:page][:per_page]
      products = Product.paginate(page: params[:page][:number], per_page: params[:page][:per_page])
    else
      products = Product.all
    end
    pagination = pagination(Product, products)
    render json: products, meta: pagination, adapter: :json
  end

  def show
    if params[:page] && params[:page][:number] && params[:page][:per_page]
      products = Product.joins(:promotions)
                        .where(promotions: { code: params[:code] })
                        .paginate(page: params[:page][:number], per_page: params[:page][:per_page])
    else
      products = Product.joins(:promotions).where(promotions: { code: params[:code] })
    end
    pagination = pagination(Product, products, "/#{params[:code]}?")
    render json: products, meta: pagination, adapter: :json
  end

  def search
    if params[:page] && params[:page][:number] && params[:page][:per_page] && params[:query]
      products = Product.search(params[:query])
                        .paginate(page: params[:page][:number], per_page: params[:page][:per_page])
    elsif params[:query]
      products = Product.search(params[:query])
    else
      products = Product.all
    end
    pagination = pagination(Product, products, "/search?query=#{params[:query]}&")
    render json: products, meta: pagination, adapter: :json
  end
end
