class Api::V1::DepartmentsController < ApplicationController
  def index
    if params[:page] && params[:page][:number] && params[:page][:per_page]
      departments = Department.paginate(page: params[:page][:number], per_page: params[:page][:per_page])
    else
      departments = Department.all
    end
    pagination = pagination(Department, departments)
    render json: departments, exclude: ['products'], meta: pagination, adapter: :json
  end

  def show
    department = Department.find(params[:id])
    render json: department, include: ['products', 'products.promotions', 'products.department'], adapter: :json
  end
end
