module Paginatable
  extend ActiveSupport::Concern

  def pagination(model, list, options = '?')
    if params[:page] && params[:page][:number] && params[:page][:per_page]
      current_page = params[:page][:number]
      per_page = params[:page][:per_page]
    else
      current_page = 1
      per_page = list.count
    end
    last_page = per_page.to_i > list.count ? 1 : (list.count / per_page.to_i).ceil
    {
      "current_page": current_page,
      "last_page": last_page,
      "next_page_url": "/#{model.name.downcase.pluralize}#{options}page[number]=#{current_page.to_i < last_page ? current_page.to_i + 1 : last_page}&page[per_page]=#{per_page}",
      "prev_page_url": "/#{model.name.downcase.pluralize}#{options}page[number]=#{current_page.to_i == 1 ? 1 : current_page.to_i - 1}&page[per_page]=#{per_page}"
    }
  end
end
