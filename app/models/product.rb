class Product < ApplicationRecord
  validates :name, :price, presence: true
  belongs_to :department
  has_and_belongs_to_many :promotions

  def self.search(query)
    if query
      where('name LIKE ?', "%#{query}%").order('id DESC')
    else
      order('id DESC')
    end
  end
end
