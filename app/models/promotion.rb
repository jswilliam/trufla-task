class Promotion < ApplicationRecord
  validates :code, :discount, presence: true
  has_and_belongs_to_many :products
end
