class ProductSerializer < ActiveModel::Serializer
  attributes :id, :name, :price, :discounted_price

  belongs_to :department
  has_many :promotions

  def discounted_price
    discounted = object.price
    object.promotions.each do |promotion|
      discounted = ((100 - promotion.discount) / 100) * discounted if promotion.active?
    end
    discounted
  end
end
