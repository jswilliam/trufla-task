class PromotionSerializer < ActiveModel::Serializer
  attributes :id, :code, :active, :discount
end
