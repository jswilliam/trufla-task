# Trufla Rails API

Trufla backend task. A rails api app.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

```
Docker
Docker Compose
```

### Installing and Running

```
docker-compose up --build
```

### API Examples

Postman Collection: 
https://www.getpostman.com/collections/dbb06b63515b8248c295

#### Paginated List of Products

```
GET /api/v1/products?page[number]=1&page[per_page]=9 HTTP/1.1
Host: localhost:3000
cache-control: no-cache
```

#### List of Products for Department

```
GET /api/v1/departments/1 HTTP/1.1
Host: localhost:3000
cache-control: no-cache
```

#### List of Products by Promocode

```
GET /api/v1/products/GreatDeal401095?page[number]=1&page[per_page]=9 HTTP/1.1
Host: localhost:3000
cache-control: no-cache

```

#### Search Products by Name

```
GET /api/v1/products/search/?query=Silk&page[number]=1&page[per_page]=9 HTTP/1.1
Host: localhost:3000
cache-control: no-cache

```

