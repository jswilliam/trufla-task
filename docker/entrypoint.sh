#! /bin/bash

bundle install
rails db:exists && rails db:migrate || rails db:setup

if [[ "$RAILS_ENV" != "development" ]]; then
  RAILS_ENV=production rails assets:precompile
fi

rm /app/tmp/pids/server.pid

puma -C config/puma.rb
