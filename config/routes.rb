Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api do
    namespace :v1 do
      resources :products, only: %i[index] do
        collection do
          get '/search', to: 'products#search'
          get '/:code', to: 'products#show'
        end
      end
      # get '/products', to: 'products#index'
      resources :departments, only: %i[index show]
    end
  end
end
